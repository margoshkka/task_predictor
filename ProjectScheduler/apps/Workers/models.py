from django.db import models

# Create your models here.


class Skill(models.Model):
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name


class Worker(models.Model):
    name = models.CharField(max_length=150)
    surname = models.CharField(max_length=150)
    skills = models.ManyToManyField(Skill, through="WorkerSkill")

    def get_worker_skills(self):
        skills = self.workerskill_set.all()
        return{
            # self.check_skill_exists(skills, 'HTML'),
            # self.check_skill_exists(skills, 'CSS'),
            # self.check_skill_exists(skills, 'JS'),
            # self.check_skill_exists(skills, 'PHP'),
            # self.check_skill_exists(skills, 'Python'),
            # self.check_skill_exists(skills, 'С'),
            # self.check_skill_exists(skills, 'Java'),
            # self.check_skill_exists(skills, 'C#'),
            # self.check_skill_exists(skills, 'Objective C'),
            # self.check_skill_exists(skills, 'ASP.net'),
            # self.check_skill_exists(skills, 'Swift'),
            # self.check_skill_exists(skills, 'Android'),
            # self.check_skill_exists(skills, 'IOS'),
            # self.check_skill_exists(skills, 'Testing'),
            # self.check_skill_exists(skills, 'SQL'),
            # self.check_skill_exists(skills, 'English'),
            'C#': self.check_skill_exists(skills, 'C#'),
            'HTML': self.check_skill_exists(skills, 'HTML'),
            'CSS': self.check_skill_exists(skills, 'CSS'),
            'JS': self.check_skill_exists(skills, 'JS'),
            'PHP': self.check_skill_exists(skills, 'PHP'),
            'Python': self.check_skill_exists(skills, 'Python'),
            'C': self.check_skill_exists(skills, 'С'),
            'Java': self.check_skill_exists(skills, 'Java'),

            'Obj.C': self.check_skill_exists(skills, 'Objective C'),
            'ASP.net': self.check_skill_exists(skills, 'ASP.net'),
            'Swift': self.check_skill_exists(skills, 'Swift'),
            'Android': self.check_skill_exists(skills, 'Android'),
            'IOS': self.check_skill_exists(skills, 'IOS'),
            'Testing': self.check_skill_exists(skills, 'Testing'),
            'SQL': self.check_skill_exists(skills, 'SQL'),
            'English': self.check_skill_exists(skills, 'English')
        }

    def get_worker(self):
        return self

    def __str__(self):
        return f'{self.id}. {self.name} {self.surname}'

    @staticmethod
    def check_skill_exists(skills, skil_name):
        var = skills.filter(skill__name=skil_name)
        return 0 if not var.exists() else var.first().level

    def to_dict(self):
        skills = self.workerskill_set.all()

        return {'id': self.id,
                'name': f'{self.name} {self.surname}',
                'HTML': self.check_skill_exists(skills, 'HTML'),
                'CSS': self.check_skill_exists(skills, 'CSS'),
                'JS': self.check_skill_exists(skills, 'JS'),
                'PHP': self.check_skill_exists(skills, 'PHP'),
                'Python': self.check_skill_exists(skills, 'Python'),
                'C': self.check_skill_exists(skills, 'С'),
                'Java': self.check_skill_exists(skills, 'Java'),
                'C#':self.check_skill_exists(skills, 'C#'),
                'Obj.C': self.check_skill_exists(skills, 'Objective C'),
                'ASP.net': self.check_skill_exists(skills, 'ASP.net'),
                'Swift': self.check_skill_exists(skills, 'Swift'),
                'Android': self.check_skill_exists(skills, 'Android'),
                'IOS': self.check_skill_exists(skills, 'IOS'),
                'Testing': self.check_skill_exists(skills, 'Testing'),
                'SQL': self.check_skill_exists(skills, 'SQL'),
                'English': self.check_skill_exists(skills, 'English'),
                }


class WorkerSkill(models.Model):
    skill = models.ForeignKey(Skill)
    worker = models.ForeignKey(Worker)
    level = models.IntegerField(default=0)

    def get_skill_name(self):
        return self.skill.name

    def get_workerskill(self):
        return self

    def get_point(self):
        return f'{self.level}'

    def __str__(self):
        return f'{self.worker} {self.skill} {self.level}'


