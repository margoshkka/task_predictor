from django.contrib import admin

from .models import Worker, Skill, WorkerSkill


class WorkerSkillInline(admin.TabularInline):
    model = WorkerSkill
    extra = 1


class WorkerAdmin(admin.ModelAdmin):
    inlines = (WorkerSkillInline,)


# Register your models here.

admin.site.register(Worker,WorkerAdmin)
admin.site.register(Skill)
admin.site.register(WorkerSkill)
