from django.apps import AppConfig


class WorkersConfig(AppConfig):
    name = 'ProjectScheduler.apps.Workers'
