from django.shortcuts import render
from django.http import HttpResponse
from .models import Worker,Skill, WorkerSkill

import random


# Create your views here.

def index(request):
    skills = Skill.objects.all()
    workers = Worker.objects.all()
    worker_skills = WorkerSkill.objects.all()
    table = []


    context = {
        'workers': workers,
        'skills': skills
    }
    return render(request, "Workers/index.html",context)
