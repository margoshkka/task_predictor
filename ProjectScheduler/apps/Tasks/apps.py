from django.apps import AppConfig


class TasksConfig(AppConfig):
    name = 'ProjectScheduler.apps.Tasks'
