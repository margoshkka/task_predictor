from django.shortcuts import render
from django.template import RequestContext
from django.forms.models import model_to_dict
from ProjectScheduler.apps.Tasks.models import Task, Requirement,TaskRequirement
from ProjectScheduler.apps.Workers.models import Worker
from ProjectScheduler.apps.Tasks.static.Tasks.static_files.py import train
import numpy as np
import sys

# Create your views here.
trainResult = train.Train
trainResult.main(trainResult)

# train.presictions results
# predictions = []


# Will segfault without this line.
# resource.setrlimit(resource.RLIMIT_STACK, [0x10000000, resource.RLIM_INFINITY])
sys.setrecursionlimit(0x100000)


def request_page(request):
    return render(request, "Tasks/index.html")


class PredictedTask:
    estimated_time = 0
    real_time = 0
    requirements = {}
    worker_id = 0
    id = 0

    def to_dict(self):

        return {'id': self.id,
                'worker_id': self.worker_id,
                'frontend': self.requirements.get('frontend'),
                'web_backend': self.requirements.get('web_backend'),
                'desktop': self.requirements.get('desktop'),
                'ios': self.requirements.get('IOS'),
                'android':self.requirements.get('Android'),
                'estimated_time': self.estimated_time,
                'time (h)': self.real_time,
                }


predicted_task = PredictedTask()


def add_selected_task(request):
    prediction = []
    if (request.POST.get('add_selected_task_button')):
        # adding tast in db (confirm worker)
        selected_row = request.POST.get('item', "")

        context = {
            'requirements': Requirement.objects.all(),
        }
        return render(request, "Tasks/add_task.html", context)

    if (request.POST.get('add_task_button')):
        context = {
            'requirements': Requirement.objects.all(),
        }
        return render(request, "Tasks/add_task.html", context)
    # prediction = request.session('prediction')
    context={
        'prediction': prediction
    }
    return render(request, "Tasks/add_selected_task.html", context)


def add_task(request):
    # prediction = request.content_params
    requirements = Requirement.objects.all()
    if (request.POST.get('add_selected_task_button')):
        # adding tast in db (confirm worker)
        prediction = request.session.get('prediction')
        worker_id = request.POST.get('worker_id', "")
        # worker_id in predictions.
        for pred in prediction:

            if pred['worker_id'] == int(worker_id):
                realT = int((float(pred['rforest']) + float(pred['etree']) + float(pred['xgb'])) / 3)
                task = Task(worker_id=worker_id,
                            # requirements=predicted_task.requirements,
                            estimatedTime=predicted_task.estimated_time,
                            realTime=realT
                            )
                task.save()

                for key,value in predicted_task.requirements.items():
                    if(value!=0):
                        requirement = requirements.filter(name=key).first()
                        task_req = TaskRequirement(task = task, requirement = requirement,
                                                   point=value)
                        task_req.save()

                trainResult.DecisionTreeRegressor['predictions'] = np.append(trainResult.DecisionTreeRegressor['predictions'],pred['dtree'])
                trainResult.ExtraTreeRegressor['predictions'] = np.append(trainResult.ExtraTreeRegressor['predictions'],pred['etree'])
                trainResult.LogisticRegression['predictions'] = np.append(trainResult.LogisticRegression['predictions'],pred['logreg'])
                trainResult.SVR['predictions'] = np.append(trainResult.SVR['predictions'],pred['svr'])
                trainResult.XGBRegressor['predictions'] = np.append(trainResult.XGBRegressor['predictions'],pred['xgb'])
                trainResult.RandomForestRegressor['predictions'] = np.append(trainResult.RandomForestRegressor['predictions'],pred['rforest'])

                index(request)
                tasks = Task.objects.all()
                scores = []
                scores.append(trainResult.get_scores(trainResult.DecisionTreeRegressor))
                scores.append(trainResult.get_scores(trainResult.ExtraTreeRegressor))
                scores.append(trainResult.get_scores(trainResult.LogisticRegression))
                scores.append(trainResult.get_scores(trainResult.RandomForestRegressor))
                scores.append(trainResult.get_scores(trainResult.XGBRegressor))
                scores.append(trainResult.get_scores(trainResult.SVR))

                context = {
                       'trainResults': trainResult,
                        'tasks': tasks,
                        'scores': scores,
                }
                return render(request, "Tasks/index.html", context)

        context = {
            'requirements': requirements
        }
        return render(request, "Tasks/add_task.html", context)

    if (request.POST.get('ok_button')):
        requirements = Requirement.objects.all()
        # predicted_task = PredictedTask()
        predicted_task.estimated_time =int(request.POST.get("estimated_time", ""))

        for item in requirements:
            point = request.POST.get(item.name, "")
            # task_req = TaskRequirement(requirement=item, point=int(point))
            predicted_task.requirements[item.name]=int(point)

        predictions = trainResult.predict(trainResult, predicted_task)
        # predictions = prediction
        dict = []
        for item in predictions:
            dict.append({
                'worker_id': int(item['worker'].id),
                'dtree': float(item['dtree']),
                'etree': float(item['etree']),
                'logreg': float(item['logreg']),
                'svr': float(item['svr']),
                'xgb': float(item['xgb']),
                'rforest': float(item['rforest']),
            })
        request.session['prediction'] = dict
            # [model_to_dict(prediction)]
        # make predictions - etree xgb rforest
        context = {
            'prediction': predictions,
        }
        return render(request, "Tasks/add_selected_task.html", context)

    context = {
        'requirements': Requirement.objects.all(),
        }

    return render(request, "Tasks/add_task.html", context)



def index(request):
    if (request.GET.get('refresh_btn')):
        trainResult.main(trainResult)

    tasks = Task.objects.all()
    scores = []

    scores.append(trainResult.get_scores(trainResult.DecisionTreeRegressor))
    scores.append(trainResult.get_scores(trainResult.ExtraTreeRegressor))
    scores.append(trainResult.get_scores(trainResult.LogisticRegression))
    scores.append(trainResult.get_scores(trainResult.RandomForestRegressor))
    scores.append(trainResult.get_scores(trainResult.XGBRegressor))
    scores.append(trainResult.get_scores(trainResult.SVR))


    context = {
        'trainResults': trainResult,
        'tasks': tasks,
        'scores': scores,
    }

    return render(request, "Tasks/index.html", context)


def tasks(request):
    tasks = Task.objects.all()

    context = {
        'tasks': tasks
    }
    return render(request,"Tasks/tasks.html",context)
