import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, cross_val_score, cross_val_predict
from sklearn.tree import DecisionTreeRegressor, ExtraTreeRegressor
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor, GradientBoostingClassifier
from xgboost import XGBRegressor
from ProjectScheduler.apps.Tasks.models import Task
from ProjectScheduler.apps.Workers.models import Worker


class Train:
    DecisionTreeRegressor = {}
    ExtraTreeRegressor = {}
    LogisticRegression = {}
    SVR = {}
    XGBRegressor = {}
    RandomForestRegressor = {}

    models = {}

    def get_scores(regressor):
        st = ''
        for item in regressor['score']:
            if item<0:
                item = -item
            st = st + item.__format__('.2f') + '\n'
        return st

    @staticmethod
    def validate(estimator, x, y, estimator_label, cv=4):
        score = cross_val_score(estimator, x, y, cv=cv, scoring='neg_mean_squared_error')
        # print(f'{estimator_label} score: {score}')

        predictions = cross_val_predict(estimator, x, y, cv=4)
        # print(predictions)
        return {
            'score': score,
            'predictions': predictions
                }

    @staticmethod
    def missing_values_table(df):
        mis_val = df.isnull().sum()
        mis_val_percent = 100 * df.isnull().sum() / len(df)
        mis_val_table = pd.concat([mis_val, mis_val_percent], axis=1)
        mis_val_table_ren_columns = mis_val_table.rename(
            columns={0: 'Missing Values', 1: '% of Total Values'})
        return mis_val_table_ren_columns

    def create_predictions_table(self):
        predictions_table = []
        for i,item in enumerate(Task.objects.all()):
            predictions_table.append({
                'tasks': item,
                'DecisionTreeRegressor': self.DecisionTreeRegressor['predictions'][i],
                'ExtraTreeRegressor': self.ExtraTreeRegressor['predictions'][i],
                'LogisticRegression': self.LogisticRegression['predictions'][i],
                'SVR': self.SVR['predictions'][i],
                'XGBRegressor': self.XGBRegressor['predictions'][i],
                'RandomForestRegressor': self.RandomForestRegressor['predictions'][i],
            })
        return predictions_table

    @staticmethod
    def main(self):

        tasks = Task.objects.all()
        workers = Worker.objects.all()

        # df_team = pd.read_csv('../input/team.csv')
        # df_tasks = pd.read_csv('../input/tasks.csv')

        var_teams = [item.to_dict() for item in list(workers)]
        var_tasks = [item.to_dict() for item in list(tasks)]

        df_team = pd.DataFrame.from_records(var_teams)
        df_tasks = pd.DataFrame.from_records(var_tasks)

        df_full = pd.merge(df_team, df_tasks, how='outer', left_on='id', right_on='worker_id',
                           suffixes=('_member', '_task'))

        # df_full = df_full.iloc[0:90]

        # print(df_full.head())

        df_full['estimated_time'] = df_full['estimated_time'].astype(float)
        y = df_full['time (h)']
        x = df_full.drop(['time (h)', 'name'], axis=1)

        dtree = DecisionTreeRegressor(random_state=0)
        self.DecisionTreeRegressor = self.validate(dtree, x, y, 'DecisionTreeRegressor')
        dtree.fit(x,y)
        self.models["dtree"] = dtree

        etree = ExtraTreeRegressor(random_state=0)
        self.ExtraTreeRegressor = self.validate (etree, x, y, 'ExtraTreeRegressor')
        etree.fit(x,y)
        self.models["etree"] = etree

        logreg = LogisticRegression(random_state=0)
        self.LogisticRegression = self.validate(logreg, x, y, 'LogisticRegression')
        logreg.fit(x,y)
        self.models["logreg"] = logreg

        svr = SVR()
        self.SVR = self.validate(svr, x, y, 'SVR')
        svr.fit(x,y)
        self.models["svr"] = svr

        xgb = XGBRegressor()
        self.XGBRegressor = self.validate(xgb, x, y, 'XGBRegressor')
        xgb.fit(x,y)
        self.models["xgb"] = xgb

        rforest = RandomForestRegressor(random_state=0)
        self.RandomForestRegressor = self.validate(rforest, x, y, 'RandomForestRegressor')
        rforest.fit(x,y)
        self.models["rforest"] = rforest


    def create_predictions_table_for_worker(self, dtree,etree, logreg, svr, xgb,rforest):

        predictions_table = []
        for i, item in enumerate(Worker.objects.all()):
            predictions_table.append({
                'worker': item,
                'dtree': dtree[i],
                'etree': etree[i],
                'logreg': logreg[i],
                'svr': svr[i],
                'xgb': xgb[i],
                'rforest': rforest[i],
            })
        return predictions_table

    @staticmethod
    def predict(self, new_task):

        workers = Worker.objects.all()

        var_teams = [item.to_dict() for item in list(workers)]
        # var_tasks = [new_task.to_dict() for i in range(len(var_teams))]

        var_tasks = []
        for item in var_teams:
            task = new_task.to_dict()
            task["worker_id"] = item["id"]
            var_tasks.append(task)

        df_team = pd.DataFrame.from_records(var_teams)
        df_tasks = pd.DataFrame.from_records(var_tasks)

        df_full = pd.merge(df_team, df_tasks, how='outer', left_on='id', right_on='worker_id',
                           suffixes=('_member', '_task'))

        # df_full = df_full.iloc[0:90]

        # print(df_full.head())

        df_full['estimated_time'] = df_full['estimated_time'].astype(float)
        y = df_full['time (h)']
        x = df_full.drop(['time (h)', 'name'], axis=1)

        results = {}
        results["dtree"] = (self.models["dtree"]).predict(x)

        results["etree"] = (self.models["etree"]).predict(x)

        results["logreg"]=(self.models["logreg"]).predict(x)

        results["svr"]= (self.models["svr"]).predict(x)

        results["xgb"]= (self.models["xgb"]).predict(x)

        results["rforest"] = (self.models["rforest"]).predict(x)

        pr_table = self.create_predictions_table_for_worker(self, results["dtree"],
                                                            results["etree"],
                                                            results["logreg"],
                                                            results["svr"],
                                                            results["xgb"],
                                                            results["rforest"]
                                                            )

        return pr_table

