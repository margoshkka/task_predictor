import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, cross_val_score, cross_val_predict
from sklearn.tree import DecisionTreeRegressor, ExtraTreeRegressor
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor, GradientBoostingClassifier
from xgboost import XGBRegressor
from ProjectScheduler.apps.Tasks.models import Task
from ProjectScheduler.apps.Workers.models import Worker


class Predict:
    DecisionTreeRegressor = {}
    ExtraTreeRegressor = {}
    LogisticRegression = {}
    SVR = {}
    XGBRegressor = {}
    RandomForestRegressor = {}

    def get_scores(regressor):
        st = ''
        for item in regressor['score']:
            st = st + item.__format__('.2f') + '\n'
        return st

    @staticmethod
    def validate(estimator, x, y, estimator_label, cv=4):
        score = cross_val_score(estimator, x, y, cv=cv, scoring='neg_mean_squared_error')
        # print(f'{estimator_label} score: {score}')

        predictions = cross_val_predict(estimator, x, y, cv=4)
        # print(predictions)
        return {
            'score': score,
            'predictions': predictions
                }

    @staticmethod
    def missing_values_table(df):
        mis_val = df.isnull().sum()
        mis_val_percent = 100 * df.isnull().sum() / len(df)
        mis_val_table = pd.concat([mis_val, mis_val_percent], axis=1)
        mis_val_table_ren_columns = mis_val_table.rename(
            columns={0: 'Missing Values', 1: '% of Total Values'})
        return mis_val_table_ren_columns

    def create_predictions_table(self):
        predictions_table = []
        for i,item in enumerate(Task.objects.all()):
            predictions_table.append({
                'tasks': item,
                'DecisionTreeRegressor': self.DecisionTreeRegressor['predictions'][i],
                'ExtraTreeRegressor': self.ExtraTreeRegressor['predictions'][i],
                'LogisticRegression': self.LogisticRegression['predictions'][i],
                'SVR': self.SVR['predictions'][i],
                'XGBRegressor': self.XGBRegressor['predictions'][i],
                'RandomForestRegressor': self.RandomForestRegressor['predictions'][i],
            })
        return predictions_table

    @staticmethod
    def main(self, new_task):

        workers = Worker.objects.all()

        var_teams = [item.to_dict() for item in list(workers)]
        var_tasks = [new_task.to_dict() for i in range(var_teams.count())]

        df_team = pd.DataFrame.from_records(var_teams)
        df_tasks = pd.DataFrame.from_records(var_tasks)

        df_full = pd.merge(df_team, df_tasks, how='outer', left_on='id', right_on='worker_id',
                           suffixes=('_member', '_task'))

        # df_full = df_full.iloc[0:90]

        # print(df_full.head())

        df_full['estimated_time'] = df_full['estimated_time'].astype(float)
        y = df_full['time (h)']
        x = df_full.drop(['time (h)', 'name'], axis=1)

        dtree = DecisionTreeRegressor(random_state=0)
        self.DecisionTreeRegressor = self.validate(dtree, x, y, 'DecisionTreeRegressor')

        etree = ExtraTreeRegressor(random_state=0)
        self.ExtraTreeRegressor = self.validate (etree, x, y, 'ExtraTreeRegressor')

        logreg = LogisticRegression(random_state=0)
        self.LogisticRegression = self.validate(logreg, x, y, 'LogisticRegression')

        svr = SVR()
        self.SVR = self.validate(svr, x, y, 'SVR')

        xgb = XGBRegressor()
        self.XGBRegressor = self.validate(xgb, x, y, 'XGBRegressor')

        rforest = RandomForestRegressor(random_state=0)
        self.RandomForestRegressor = self.validate(rforest, x, y, 'RandomForestRegressor')


