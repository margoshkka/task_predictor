from django.contrib import admin

from .models import Task, TaskRequirement, Requirement

class TaskRequirementInline(admin.TabularInline):
    model = TaskRequirement
    extra = 1


class TaskAdmin(admin.ModelAdmin):
    inlines = (TaskRequirementInline,)


# Register your models here.

admin.site.register(Task,TaskAdmin)
admin.site.register(Requirement)
admin.site.register(TaskRequirement)

