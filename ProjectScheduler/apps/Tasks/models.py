from django.db import models
from ProjectScheduler.apps.Workers.models import Worker, Skill
# Create your models here.


class Requirement(models.Model):
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name


class Task(models.Model):
    worker = models.ForeignKey(Worker)
    requirements = models.ManyToManyField(Requirement
                                          , through="TaskRequirement")
    estimatedTime = models.IntegerField()
    realTime = models.IntegerField()

    def get_task_with_worker(self):
        return self

    def get_task(self):
        return [self.requirements, self.estimatedTime, self.realTime]

    def __str__(self):
        return f'{self.id}.{list(self.requirements.all())} - {self.worker} - {self.realTime}'

    @staticmethod
    def check_requirement_exists(requirements, req_name):
        var = requirements.filter(requirement__name=req_name)
        return 0 if not var.exists() else var.first().point

    def to_dict(self):
        requirements = self.taskrequirement_set.all()

        return {'id': self.id,
                'worker_id': self.worker.id,
                'frontend': self.check_requirement_exists(requirements, 'frontend'),
                'web_backend': self.check_requirement_exists(requirements, 'web_backend'),
                'desktop': self.check_requirement_exists(requirements, 'desktop'),
                'ios': self.check_requirement_exists(requirements, 'IOS'),
                'android':self.check_requirement_exists(requirements, 'Android'),
                'estimated_time': self.estimatedTime,
                'time (h)': self.realTime,
                }


class TaskRequirement(models.Model):
    requirement = models.ForeignKey(Requirement)
    task = models.ForeignKey(Task)
    point = models.IntegerField()

    def __str__(self):
        return f'{self.requirement} - {self.point}'

